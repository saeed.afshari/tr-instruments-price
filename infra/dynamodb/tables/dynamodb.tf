resource "aws_dynamodb_table" "instruments" {
  name = "instruments"
  billing_mode = "PAY_PER_REQUEST"
  hash_key = "isin"
  attribute {
    name = "isin"
    type = "S"
  }
}

resource "aws_dynamodb_table" "quotes" {
  name = "quotes"
  billing_mode = "PAY_PER_REQUEST"
  hash_key = "id"
  attribute {
    name = "id"
    type = "S"
  }
  attribute {
    name = "isin"
    type = "S"
  }
  attribute {
    name = "openingTime"
    type = "S"
  }
  global_secondary_index {
    name = "isin_opening_time_gsi"
    hash_key = "isin"
    range_key = "openingTime"
    projection_type = "ALL"
  }
}
