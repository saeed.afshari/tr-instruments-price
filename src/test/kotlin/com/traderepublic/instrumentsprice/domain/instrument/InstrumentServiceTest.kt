package com.traderepublic.instrumentsprice.domain.instrument

import io.mockk.*
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test

internal class InstrumentServiceTest {

    private val instrumentEntity = InstrumentEntity("1", description = "a")
    private val instrumentDto = InstrumentDto(isin = "1", description = "a")

    @Test
    fun `calls once InstrumentRepository#save`() {
        val instrumentRepository = mockk<InstrumentRepository> {
            every { save(any()) } returns mockk(relaxed = true)
        }
        val instrumentService = InstrumentService(instrumentRepository)

        instrumentService.save(instrumentDto)

        verify(exactly = 1) {
            instrumentRepository.save(any())
        }
    }

    @Test
    fun `uses correct instrumentEntity when calls InstrumentRepository#save`() {
        val instrumentEntitySlot = slot<InstrumentEntity>()
        val instrumentRepository = mockk<InstrumentRepository> {
            every { save(capture(instrumentEntitySlot)) } returns mockk(relaxed = true)
        }
        val instrumentService = InstrumentService(instrumentRepository)

        instrumentService.save(instrumentDto)

        verify(exactly = 1) {
            instrumentRepository.save(any())
        }
        assertEquals(instrumentEntity, instrumentEntitySlot.captured)
    }

    @Test
    fun `returns correct instrumentDto`() {
        val instrumentRepository = mockk<InstrumentRepository> {
            every { save(instrumentEntity) } returns instrumentEntity
        }
        val instrumentService = InstrumentService(instrumentRepository)

        val actual = instrumentService.save(instrumentDto)

        assertEquals(instrumentDto, actual)
    }

    @Test
    fun `calls once InstrumentRepository#deleteById`() {
        val instrumentRepository = mockk<InstrumentRepository> {
            every { deleteById(any()) } just Runs
        }
        val instrumentService = InstrumentService(instrumentRepository)

        instrumentService.deleteByISIN("1")

        verify(exactly = 1) {
            instrumentRepository.deleteById("1")
        }
    }

    @Test
    fun `uses correct isin when calls InstrumentRepository#deleteById`() {
        val isinSlot = slot<String>()
        val instrumentRepository = mockk<InstrumentRepository> {
            every { deleteById(capture(isinSlot)) } just Runs
        }
        val instrumentService = InstrumentService(instrumentRepository)

        instrumentService.deleteByISIN("1")

        assertEquals("1", isinSlot.captured)
    }
}
