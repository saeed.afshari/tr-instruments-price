package com.traderepublic.instrumentsprice.domain.quote

import io.mockk.*
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test
import java.math.BigDecimal
import java.time.LocalDateTime
import java.util.*

internal class QuoteServiceTest {
    private val quoteDto = QuoteDto(
        id = UUID.randomUUID().toString(),
        isin = "1",
        openingTime = LocalDateTime.now(),
        price = BigDecimal.valueOf(1.00),
        createdAt = LocalDateTime.now()
    )
    private val quoteEntity = QuoteEntity(quoteDto)

    @Test
    fun `calls once QuoteRepository#save`() {
        val quoteRepository = mockk<QuoteRepository> {
            every { save(any()) } returns mockk(relaxed = true)
        }
        val quoteService = QuoteService(quoteRepository)

        quoteService.save(quoteDto)

        verify(exactly = 1) {
            quoteRepository.save(any())
        }
    }

    @Test
    fun `uses correct quoteEntity when calls QuoteRepository#save`() {
        val quoteSlot = slot<QuoteEntity>()
        val quoteRepository = mockk<QuoteRepository> {
            every { save(capture(quoteSlot)) } returns mockk(relaxed = true)
        }
        val quoteService = QuoteService(quoteRepository)

        quoteService.save(quoteDto)

        val expected = quoteEntity.copy(openingTime = quoteEntity.openingTime.withSecond(0).withNano(0))
        Assertions.assertEquals(expected, quoteSlot.captured)
    }

    @Test
    fun `returns correct quoteDto`() {
        val quoteRepository = mockk<QuoteRepository> {
            every { save(any()) } returns quoteEntity
        }
        val quoteService = QuoteService(quoteRepository)

        val actual = quoteService.save(quoteDto)

        Assertions.assertEquals(quoteDto, actual)
    }

    @Test
    fun `calls once QuoteRepository#deleteByISIN`() {
        val quoteRepository = mockk<QuoteRepository> {
            every { deleteByIsin(any()) } just Runs
        }
        val quoteService = QuoteService(quoteRepository)

        quoteService.deleteByISIN("1")

        verify(exactly = 1) { quoteRepository.deleteByIsin(any()) }
    }

    @Test
    fun `uses correct isin when calls QuoteRepository#findByIsinAndOpeningTime`() {
        val isinSlot = slot<String>()
        val quoteRepository = mockk<QuoteRepository> {
            every { findByIsinAndOpeningTime(capture(isinSlot), any()) } returns emptyList()
        }
        val quoteService = QuoteService(quoteRepository)

        quoteService.findAllByIsinAndOpeningTime("1", mockk())

        verify(exactly = 1) {
            quoteRepository.findByIsinAndOpeningTime(any(), any())
        }
        Assertions.assertEquals("1", isinSlot.captured)
    }

    @Test
    fun `uses correct openingTime when calls QuoteRepository#findByIsinAndOpeningTime`() {
        val openingTimeSlot = slot<LocalDateTime>()
        val openingTime = LocalDateTime.now()
        val quoteRepository = mockk<QuoteRepository> {
            every { findByIsinAndOpeningTime(any(), capture(openingTimeSlot)) } returns emptyList()
        }
        val quoteService = QuoteService(quoteRepository)

        quoteService.findAllByIsinAndOpeningTime("1", openingTime)

        verify(exactly = 1) {
            quoteRepository.findByIsinAndOpeningTime(any(), any())
        }
        Assertions.assertEquals(openingTime, openingTimeSlot.captured)
    }

    @Test
    fun `returns correct result when calls QuoteRepository#findByIsinAndOpeningTime`() {
        val quoteRepository = mockk<QuoteRepository> {
            every { findByIsinAndOpeningTime(any(), any()) } returns listOf(quoteEntity)
        }
        val quoteService = QuoteService(quoteRepository)

        val actual = quoteService.findAllByIsinAndOpeningTime("1", LocalDateTime.now())

        Assertions.assertEquals(listOf(quoteDto), actual)
    }
}
