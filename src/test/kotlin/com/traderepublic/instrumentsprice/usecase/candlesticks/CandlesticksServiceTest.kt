package com.traderepublic.instrumentsprice.usecase.candlesticks

import com.traderepublic.instrumentsprice.domain.quote.Quote
import com.traderepublic.instrumentsprice.domain.quote.QuoteDto
import io.mockk.every
import io.mockk.mockk
import io.mockk.slot
import io.mockk.verify
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.assertThrows
import java.math.BigDecimal
import java.time.LocalDateTime
import java.time.LocalDateTime.parse
import java.util.*

internal class CandlesticksServiceTest {

    @Test
    fun `throws if getCandlesticksCommandDto#window is not THIRTY_MINUTES`() {
        val quoteService = mockk<Quote>()
        val candlesticksService = CandlesticksService(quoteService)
        val getCandlesticksCommandDto = GetCandlesticksCommandDto(
            isin = "1",
            since = mockk(),
            window = CandlesticksWindow.FIFTEEN_MINUTES
        )

        val exception = assertThrows<IllegalArgumentException> {
            candlesticksService.getCandlesticks(getCandlesticksCommandDto)
        }

        assertEquals("Candlesticks in the specified window timeframe is not implemented yet.", exception.message)
    }

    @Test
    fun `calls 30 times QuoteService#findAllByIsinAndOpeningTime`() {
        val since = parse("2022-02-21T02:44:48.697256")
        val quoteService = mockk<Quote>(relaxed = true)
        val candlesticksService = CandlesticksService(quoteService)
        val getCandlesticksCommandDto = GetCandlesticksCommandDto(
            isin = "1",
            since = since,
            window = CandlesticksWindow.THIRTY_MINUTES
        )

        candlesticksService.getCandlesticks(getCandlesticksCommandDto)

        verify(exactly = 30) { quoteService.findAllByIsinAndOpeningTime(any(), any()) }
    }

    @Test
    fun `uses correct isin when calls QuoteService#findAllByIsinAndOpeningTime`() {
        val since = parse("2022-02-21T02:44:48.697256")
        val isinSlot = slot<String>()
        val quoteService = mockk<Quote> {
            every { findAllByIsinAndOpeningTime(capture(isinSlot), any()) } returns mockk(relaxed = true)
        }
        val candlesticksService = CandlesticksService(quoteService)
        val getCandlesticksCommandDto = GetCandlesticksCommandDto(
            isin = "1",
            since = since,
            window = CandlesticksWindow.THIRTY_MINUTES
        )

        assertThrows<NoSuchElementException> {
            candlesticksService.getCandlesticks(getCandlesticksCommandDto)
        }

        assertEquals("1", isinSlot.captured)
    }

    @Test
    fun `uses correct openingTime when calls QuoteService#findAllByIsinAndOpeningTime`() {
        val since = parse("2022-02-21T02:44:48.697256")
        val openingsTimeSlot = mutableListOf<LocalDateTime>()
        val quoteService = mockk<Quote> {
            every { findAllByIsinAndOpeningTime(any(), capture(openingsTimeSlot)) } returns mockk(relaxed = true)
        }
        val candlesticksService = CandlesticksService(quoteService)
        val getCandlesticksCommandDto = GetCandlesticksCommandDto(
            isin = "1",
            since = since,
            window = CandlesticksWindow.THIRTY_MINUTES
        )

        assertThrows<NoSuchElementException> {
            candlesticksService.getCandlesticks(getCandlesticksCommandDto)
        }

        assertEquals(
            listOf(
                parse("2022-02-21T02:44"),
                parse("2022-02-21T02:45"),
                parse("2022-02-21T02:46"),
                parse("2022-02-21T02:47"),
                parse("2022-02-21T02:48"),
                parse("2022-02-21T02:49"),
                parse("2022-02-21T02:50"),
                parse("2022-02-21T02:51"),
                parse("2022-02-21T02:52"),
                parse("2022-02-21T02:53"),
                parse("2022-02-21T02:54"),
                parse("2022-02-21T02:55"),
                parse("2022-02-21T02:56"),
                parse("2022-02-21T02:57"),
                parse("2022-02-21T02:58"),
                parse("2022-02-21T02:59"),
                parse("2022-02-21T03:00"),
                parse("2022-02-21T03:01"),
                parse("2022-02-21T03:02"),
                parse("2022-02-21T03:03"),
                parse("2022-02-21T03:04"),
                parse("2022-02-21T03:05"),
                parse("2022-02-21T03:06"),
                parse("2022-02-21T03:07"),
                parse("2022-02-21T03:08"),
                parse("2022-02-21T03:09"),
                parse("2022-02-21T03:10"),
                parse("2022-02-21T03:11"),
                parse("2022-02-21T03:12"),
                parse("2022-02-21T03:13")
            ),
            openingsTimeSlot
        )
    }

    @Test
    fun `returns correct candlesticks`() {
        val since = parse("2022-02-21T02:44:48.697256")
        val quoteService = mockk<Quote>()
        (0L..29).forEach { minute ->
            every {
                quoteService.findAllByIsinAndOpeningTime(
                    "1",
                    since.toHourAndMinuteFormat().plusMinutes(minute)
                )
            } returns listOf(
                QuoteDto(
                    id = UUID.randomUUID().toString(),
                    isin = "1",
                    openingTime = since.plusMinutes(minute),
                    price = BigDecimal.valueOf(minute),
                    createdAt = since.plusMinutes(minute)
                )
            )
        }
        val candlesticksService = CandlesticksService(quoteService)
        val getCandlesticksCommandDto = GetCandlesticksCommandDto(
            isin = "1",
            since = since,
            window = CandlesticksWindow.THIRTY_MINUTES
        )

        val candlesticks = candlesticksService.getCandlesticks(getCandlesticksCommandDto)

        val expected = listOf(
            CandlesticksDto(
                openTimestamp = parse("2022-02-21T02:44"),
                openPrice = BigDecimal.valueOf(0),
                highPrice = BigDecimal.valueOf(0),
                lowPrice = BigDecimal.valueOf(0),
                closePrice = BigDecimal.valueOf(0),
                closeTimestamp = parse("2022-02-21T02:45")
            ),
            CandlesticksDto(
                openTimestamp = parse("2022-02-21T02:45"),
                openPrice = BigDecimal.valueOf(1),
                highPrice = BigDecimal.valueOf(1),
                lowPrice = BigDecimal.valueOf(1),
                closePrice = BigDecimal.valueOf(1),
                closeTimestamp = parse("2022-02-21T02:46")
            ),
            CandlesticksDto(
                openTimestamp = parse("2022-02-21T02:46"),
                openPrice = BigDecimal.valueOf(2),
                highPrice = BigDecimal.valueOf(2),
                lowPrice = BigDecimal.valueOf(2),
                closePrice = BigDecimal.valueOf(2),
                closeTimestamp = parse("2022-02-21T02:47")
            ),
            CandlesticksDto(
                openTimestamp = parse("2022-02-21T02:47"),
                openPrice = BigDecimal.valueOf(3),
                highPrice = BigDecimal.valueOf(3),
                lowPrice = BigDecimal.valueOf(3),
                closePrice = BigDecimal.valueOf(3),
                closeTimestamp = parse("2022-02-21T02:48")
            ),
            CandlesticksDto(
                openTimestamp = parse("2022-02-21T02:48"),
                openPrice = BigDecimal.valueOf(4),
                highPrice = BigDecimal.valueOf(4),
                lowPrice = BigDecimal.valueOf(4),
                closePrice = BigDecimal.valueOf(4),
                closeTimestamp = parse("2022-02-21T02:49")
            ),
            CandlesticksDto(
                openTimestamp = parse("2022-02-21T02:49"),
                openPrice = BigDecimal.valueOf(5),
                highPrice = BigDecimal.valueOf(5),
                lowPrice = BigDecimal.valueOf(5),
                closePrice = BigDecimal.valueOf(5),
                closeTimestamp = parse("2022-02-21T02:50")
            ),
            CandlesticksDto(
                openTimestamp = parse("2022-02-21T02:50"),
                openPrice = BigDecimal.valueOf(6),
                highPrice = BigDecimal.valueOf(6),
                lowPrice = BigDecimal.valueOf(6),
                closePrice = BigDecimal.valueOf(6),
                closeTimestamp = parse("2022-02-21T02:51")
            ),
            CandlesticksDto(
                openTimestamp = parse("2022-02-21T02:51"),
                openPrice = BigDecimal.valueOf(7),
                highPrice = BigDecimal.valueOf(7),
                lowPrice = BigDecimal.valueOf(7),
                closePrice = BigDecimal.valueOf(7),
                closeTimestamp = parse("2022-02-21T02:52")
            ),
            CandlesticksDto(
                openTimestamp = parse("2022-02-21T02:52"),
                openPrice = BigDecimal.valueOf(8),
                highPrice = BigDecimal.valueOf(8),
                lowPrice = BigDecimal.valueOf(8),
                closePrice = BigDecimal.valueOf(8),
                closeTimestamp = parse("2022-02-21T02:53")
            ),
            CandlesticksDto(
                openTimestamp = parse("2022-02-21T02:53"),
                openPrice = BigDecimal.valueOf(9),
                highPrice = BigDecimal.valueOf(9),
                lowPrice = BigDecimal.valueOf(9),
                closePrice = BigDecimal.valueOf(9),
                closeTimestamp = parse("2022-02-21T02:54")
            ),
            CandlesticksDto(
                openTimestamp = parse("2022-02-21T02:54"),
                openPrice = BigDecimal.valueOf(10),
                highPrice = BigDecimal.valueOf(10),
                lowPrice = BigDecimal.valueOf(10),
                closePrice = BigDecimal.valueOf(10),
                closeTimestamp = parse("2022-02-21T02:55")
            ),
            CandlesticksDto(
                openTimestamp = parse("2022-02-21T02:55"),
                openPrice = BigDecimal.valueOf(11),
                highPrice = BigDecimal.valueOf(11),
                lowPrice = BigDecimal.valueOf(11),
                closePrice = BigDecimal.valueOf(11),
                closeTimestamp = parse("2022-02-21T02:56")
            ),
            CandlesticksDto(
                openTimestamp = parse("2022-02-21T02:56"),
                openPrice = BigDecimal.valueOf(12),
                highPrice = BigDecimal.valueOf(12),
                lowPrice = BigDecimal.valueOf(12),
                closePrice = BigDecimal.valueOf(12),
                closeTimestamp = parse("2022-02-21T02:57")
            ),
            CandlesticksDto(
                openTimestamp = parse("2022-02-21T02:57"),
                openPrice = BigDecimal.valueOf(13),
                highPrice = BigDecimal.valueOf(13),
                lowPrice = BigDecimal.valueOf(13),
                closePrice = BigDecimal.valueOf(13),
                closeTimestamp = parse("2022-02-21T02:58")
            ),
            CandlesticksDto(
                openTimestamp = parse("2022-02-21T02:58"),
                openPrice = BigDecimal.valueOf(14),
                highPrice = BigDecimal.valueOf(14),
                lowPrice = BigDecimal.valueOf(14),
                closePrice = BigDecimal.valueOf(14),
                closeTimestamp = parse("2022-02-21T02:59")
            ),
            CandlesticksDto(
                openTimestamp = parse("2022-02-21T02:59"),
                openPrice = BigDecimal.valueOf(15),
                highPrice = BigDecimal.valueOf(15),
                lowPrice = BigDecimal.valueOf(15),
                closePrice = BigDecimal.valueOf(15),
                closeTimestamp = parse("2022-02-21T03:00")
            ),
            CandlesticksDto(
                openTimestamp = parse("2022-02-21T03:00"),
                openPrice = BigDecimal.valueOf(16),
                highPrice = BigDecimal.valueOf(16),
                lowPrice = BigDecimal.valueOf(16),
                closePrice = BigDecimal.valueOf(16),
                closeTimestamp = parse("2022-02-21T03:01")
            ),
            CandlesticksDto(
                openTimestamp = parse("2022-02-21T03:01"),
                openPrice = BigDecimal.valueOf(17),
                highPrice = BigDecimal.valueOf(17),
                lowPrice = BigDecimal.valueOf(17),
                closePrice = BigDecimal.valueOf(17),
                closeTimestamp = parse("2022-02-21T03:02")
            ),
            CandlesticksDto(
                openTimestamp = parse("2022-02-21T03:02"),
                openPrice = BigDecimal.valueOf(18),
                highPrice = BigDecimal.valueOf(18),
                lowPrice = BigDecimal.valueOf(18),
                closePrice = BigDecimal.valueOf(18),
                closeTimestamp = parse("2022-02-21T03:03")
            ),
            CandlesticksDto(
                openTimestamp = parse("2022-02-21T03:03"),
                openPrice = BigDecimal.valueOf(19),
                highPrice = BigDecimal.valueOf(19),
                lowPrice = BigDecimal.valueOf(19),
                closePrice = BigDecimal.valueOf(19),
                closeTimestamp = parse("2022-02-21T03:04")
            ),
            CandlesticksDto(
                openTimestamp = parse("2022-02-21T03:04"),
                openPrice = BigDecimal.valueOf(20),
                highPrice = BigDecimal.valueOf(20),
                lowPrice = BigDecimal.valueOf(20),
                closePrice = BigDecimal.valueOf(20),
                closeTimestamp = parse("2022-02-21T03:05")
            ),
            CandlesticksDto(
                openTimestamp = parse("2022-02-21T03:05"),
                openPrice = BigDecimal.valueOf(21),
                highPrice = BigDecimal.valueOf(21),
                lowPrice = BigDecimal.valueOf(21),
                closePrice = BigDecimal.valueOf(21),
                closeTimestamp = parse("2022-02-21T03:06")
            ),
            CandlesticksDto(
                openTimestamp = parse("2022-02-21T03:06"),
                openPrice = BigDecimal.valueOf(22),
                highPrice = BigDecimal.valueOf(22),
                lowPrice = BigDecimal.valueOf(22),
                closePrice = BigDecimal.valueOf(22),
                closeTimestamp = parse("2022-02-21T03:07")
            ),
            CandlesticksDto(
                openTimestamp = parse("2022-02-21T03:07"),
                openPrice = BigDecimal.valueOf(23),
                highPrice = BigDecimal.valueOf(23),
                lowPrice = BigDecimal.valueOf(23),
                closePrice = BigDecimal.valueOf(23),
                closeTimestamp = parse("2022-02-21T03:08")
            ),
            CandlesticksDto(
                openTimestamp = parse("2022-02-21T03:08"),
                openPrice = BigDecimal.valueOf(24),
                highPrice = BigDecimal.valueOf(24),
                lowPrice = BigDecimal.valueOf(24),
                closePrice = BigDecimal.valueOf(24),
                closeTimestamp = parse("2022-02-21T03:09")
            ),
            CandlesticksDto(
                openTimestamp = parse("2022-02-21T03:09"),
                openPrice = BigDecimal.valueOf(25),
                highPrice = BigDecimal.valueOf(25),
                lowPrice = BigDecimal.valueOf(25),
                closePrice = BigDecimal.valueOf(25),
                closeTimestamp = parse("2022-02-21T03:10")
            ),
            CandlesticksDto(
                openTimestamp = parse("2022-02-21T03:10"),
                openPrice = BigDecimal.valueOf(26),
                highPrice = BigDecimal.valueOf(26),
                lowPrice = BigDecimal.valueOf(26),
                closePrice = BigDecimal.valueOf(26),
                closeTimestamp = parse("2022-02-21T03:11")
            ),
            CandlesticksDto(
                openTimestamp = parse("2022-02-21T03:11"),
                openPrice = BigDecimal.valueOf(27),
                highPrice = BigDecimal.valueOf(27),
                lowPrice = BigDecimal.valueOf(27),
                closePrice = BigDecimal.valueOf(27),
                closeTimestamp = parse("2022-02-21T03:12")
            ),
            CandlesticksDto(
                openTimestamp = parse("2022-02-21T03:12"),
                openPrice = BigDecimal.valueOf(28),
                highPrice = BigDecimal.valueOf(28),
                lowPrice = BigDecimal.valueOf(28),
                closePrice = BigDecimal.valueOf(28),
                closeTimestamp = parse("2022-02-21T03:13")
            ),
            CandlesticksDto(
                openTimestamp = parse("2022-02-21T03:13"),
                openPrice = BigDecimal.valueOf(29),
                highPrice = BigDecimal.valueOf(29),
                lowPrice = BigDecimal.valueOf(29),
                closePrice = BigDecimal.valueOf(29),
                closeTimestamp = parse("2022-02-21T03:14")
            )
        )
        assertEquals(expected, candlesticks)
    }

    @Test
    fun `repeats the only available candlesticks if only The first minute candlesticks quotas are available`() {
        val since = parse("2022-02-21T02:44:48.697256")
        val quoteService = mockk<Quote>()
        val missingMinutes = (1L..29)
        (0L..29).forEach { minute ->
            if (!missingMinutes.contains(minute)) {
                every {
                    quoteService.findAllByIsinAndOpeningTime(
                        "1",
                        since.toHourAndMinuteFormat().plusMinutes(minute)
                    )
                } returns listOf(
                    QuoteDto(
                        id = UUID.randomUUID().toString(),
                        isin = "1",
                        openingTime = since.plusMinutes(minute),
                        price = BigDecimal.valueOf(minute),
                        createdAt = since.plusMinutes(minute)
                    )
                )
            } else {
                every {
                    quoteService.findAllByIsinAndOpeningTime(
                        "1",
                        since.toHourAndMinuteFormat().plusMinutes(minute)
                    )
                } returns emptyList()
            }
        }
        val candlesticksService = CandlesticksService(quoteService)
        val getCandlesticksCommandDto = GetCandlesticksCommandDto(
            isin = "1",
            since = since,
            window = CandlesticksWindow.THIRTY_MINUTES
        )

        val candlesticks = candlesticksService.getCandlesticks(getCandlesticksCommandDto)

        val expected = mutableListOf<CandlesticksDto>()
        repeat(30) {
            expected.add(
                CandlesticksDto(
                    openTimestamp = parse("2022-02-21T02:44"),
                    openPrice = BigDecimal.valueOf(0),
                    highPrice = BigDecimal.valueOf(0),
                    lowPrice = BigDecimal.valueOf(0),
                    closePrice = BigDecimal.valueOf(0),
                    closeTimestamp = parse("2022-02-21T02:45")
                )
            )
        }
        assertEquals(expected, candlesticks)
    }

    @Test
    fun `skips missing candlesticks`() {
        val since = parse("2022-02-21T02:44:48.697256")
        val quoteService = mockk<Quote>()
        val missingMinutes = (0L..28)
        (0L..29).forEach { minute ->
            if (!missingMinutes.contains(minute)) {
                every {
                    quoteService.findAllByIsinAndOpeningTime(
                        "1",
                        since.toHourAndMinuteFormat().plusMinutes(minute)
                    )
                } returns listOf(
                    QuoteDto(
                        id = UUID.randomUUID().toString(),
                        isin = "1",
                        openingTime = since.plusMinutes(minute),
                        price = BigDecimal.valueOf(minute),
                        createdAt = since.plusMinutes(minute)
                    )
                )
            } else {
                every {
                    quoteService.findAllByIsinAndOpeningTime(
                        "1",
                        since.toHourAndMinuteFormat().plusMinutes(minute)
                    )
                } returns emptyList()
            }
        }
        val candlesticksService = CandlesticksService(quoteService)
        val getCandlesticksCommandDto = GetCandlesticksCommandDto(
            isin = "1",
            since = since,
            window = CandlesticksWindow.THIRTY_MINUTES
        )

        val candlesticks = candlesticksService.getCandlesticks(getCandlesticksCommandDto)

        val expected = listOf(
            CandlesticksDto(
                openTimestamp = parse("2022-02-21T03:13"),
                openPrice = BigDecimal.valueOf(29),
                highPrice = BigDecimal.valueOf(29),
                lowPrice = BigDecimal.valueOf(29),
                closePrice = BigDecimal.valueOf(29),
                closeTimestamp = parse("2022-02-21T03:14")
            )
        )
        assertEquals(expected, candlesticks)
    }

    private fun LocalDateTime.toHourAndMinuteFormat(): LocalDateTime = withSecond(0).withNano(0)
}
