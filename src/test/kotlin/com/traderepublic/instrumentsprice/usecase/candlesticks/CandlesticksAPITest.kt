package com.traderepublic.instrumentsprice.usecase.candlesticks

import com.ninjasquad.springmockk.MockkBean
import io.mockk.every
import io.mockk.verify
import org.hamcrest.Matchers
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.http.MediaType
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders
import org.springframework.test.web.servlet.result.MockMvcResultMatchers
import java.math.BigDecimal
import java.time.LocalDateTime

@SpringBootTest
@AutoConfigureMockMvc(addFilters = false)
internal class CandlesticksAPITest {

    @Autowired
    private lateinit var mockMvc: MockMvc

    @MockkBean
    private lateinit var candlesticksService: CandlesticksService

    @Test
    fun getLast30MinutesCandleSticks() {
        val openTimestamp = LocalDateTime.parse("2022-02-21T02:44:48.697256")
        every { candlesticksService.getCandlesticks(any()) } returns listOf(
            CandlesticksDto(
                openTimestamp,
                openPrice = BigDecimal.valueOf(10),
                highPrice = BigDecimal.valueOf(20),
                lowPrice = BigDecimal.valueOf(5),
                closePrice = BigDecimal.valueOf(18),
                closeTimestamp = openTimestamp.plusMinutes(1)
            )
        )

        mockMvc.perform(MockMvcRequestBuilders.get("/api/v1/candlesticks/1"))
            .andExpect(MockMvcResultMatchers.status().isOk)
            .andExpect(MockMvcResultMatchers.jsonPath("$", Matchers.hasSize<Int>(1)))
            .andExpect(MockMvcResultMatchers.content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(
                MockMvcResultMatchers.content()
                    .json(
                        ClassLoader.getSystemResource("usecase/candlesticks/get-last-30-minutes-candlesticks.json")
                            .readText()
                    )
            )
        verify(exactly = 1) { candlesticksService.getCandlesticks(any()) }
    }
}
