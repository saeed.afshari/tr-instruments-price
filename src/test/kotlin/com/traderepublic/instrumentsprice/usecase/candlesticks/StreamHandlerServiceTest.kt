package com.traderepublic.instrumentsprice.usecase.candlesticks

import org.junit.jupiter.api.Test

internal class StreamHandlerServiceTest {

    @Test
    fun `calls once InstrumentStreamService#connect`() {

    }

    @Test
    fun `uses correct callback when calls InstrumentStreamService#connect`() {

    }

    @Test
    fun `calls once QuoteStreamService#connect`() {

    }

    @Test
    fun `uses correct callback when calls QuoteStreamService#connect`() {

    }

    @Test
    fun `calls once InstrumentService#save when an event with ADD type is received`() {

    }

    @Test
    fun `uses correct value when calls InstrumentService#save`() {

    }

    @Test
    fun `calls once InstrumentService#deleteByISIN when an event with DELETE type is received`() {

    }

    @Test
    fun `calls once QuoteService#deleteByISIN when an event with DELETE type is received`() {

    }

    @Test
    fun `uses correct value when calls InstrumentService#deleteByISIN`() {

    }

    @Test
    fun `uses correct value when calls QuoteService#deleteByISIN`() {

    }

    @Test
    fun `calls once QuoteService#save when an event is received`() {

    }
}
