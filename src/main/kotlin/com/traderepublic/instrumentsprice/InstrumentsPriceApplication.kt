package com.traderepublic.instrumentsprice

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication


@SpringBootApplication
class InstrumentsPriceApplication

fun main(args: Array<String>) {
    runApplication<InstrumentsPriceApplication>(*args)
}
