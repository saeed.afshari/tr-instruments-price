package com.traderepublic.instrumentsprice.domain.instrument

import com.traderepublic.instrumentsprice.shared.NoArgsConstructor

@NoArgsConstructor
data class InstrumentDto(
    val isin: String,
    val description: String
) {
    constructor(instrumentEntity: InstrumentEntity) : this(
        isin = instrumentEntity.isin,
        description = instrumentEntity.description
    )
}
