package com.traderepublic.instrumentsprice.domain.instrument

import org.socialsignin.spring.data.dynamodb.repository.DynamoDBCrudRepository

interface InstrumentRepository : DynamoDBCrudRepository<InstrumentEntity, String>
