package com.traderepublic.instrumentsprice.domain.instrument

import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBHashKey
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBTable
import com.traderepublic.instrumentsprice.shared.NoArgsConstructor

@NoArgsConstructor
@DynamoDBTable(tableName = "instruments")
data class InstrumentEntity(
    @DynamoDBHashKey
    var isin: String,
    var description: String
) {
    constructor(instrumentDto: InstrumentDto) : this(
        isin = instrumentDto.isin,
        description = instrumentDto.description
    )
}
