package com.traderepublic.instrumentsprice.domain.instrument

import org.springframework.stereotype.Service

@Service
class InstrumentService(
    private val instrumentRepository: InstrumentRepository
) : Instrument {

    override fun save(instrumentDto: InstrumentDto): InstrumentDto {
        val instrumentEntity = InstrumentEntity(instrumentDto)
        val result = instrumentRepository.save(instrumentEntity)
        return InstrumentDto(result)
    }

    override fun deleteByISIN(isin: String) = instrumentRepository.deleteById(isin)
}
