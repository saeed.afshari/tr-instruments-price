package com.traderepublic.instrumentsprice.domain.instrument

interface Instrument {
    fun save(instrumentDto: InstrumentDto): InstrumentDto
    fun deleteByISIN(isin: String)
}
