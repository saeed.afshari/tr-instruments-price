package com.traderepublic.instrumentsprice.domain.quote

import org.springframework.stereotype.Service
import java.time.LocalDateTime

@Service
class QuoteService(
    private val quoteRepository: QuoteRepository
) : Quote {

    override fun save(quoteDto: QuoteDto): QuoteDto {
        val quoteEntity = QuoteEntity(quoteDto.copy(openingTime = quoteDto.openingTime.withSecond(0).withNano(0)))
        val result = quoteRepository.save(quoteEntity)
        return QuoteDto(result)
    }

    override fun deleteByISIN(isin: String) = quoteRepository.deleteByIsin(isin)

    override fun findAllByIsinAndOpeningTime(isin: String, openingTime: LocalDateTime): List<QuoteDto> =
        quoteRepository.findByIsinAndOpeningTime(isin, openingTime).map { QuoteDto(it) }
}
