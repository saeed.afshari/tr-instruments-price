package com.traderepublic.instrumentsprice.domain.quote

import com.amazonaws.services.dynamodbv2.datamodeling.*
import com.traderepublic.instrumentsprice.shared.DynamoDbLocalDateTimeConverter
import com.traderepublic.instrumentsprice.shared.NoArgsConstructor
import java.math.BigDecimal
import java.time.LocalDateTime

@NoArgsConstructor
@DynamoDBTable(tableName = "quotes")
data class QuoteEntity(
    @DynamoDBHashKey
    var id: String,
    @get:DynamoDBIndexHashKey(globalSecondaryIndexName = "isin_opening_time_gsi")
    var isin: String,
    @get:DynamoDBIndexRangeKey(globalSecondaryIndexName = "isin_opening_time_gsi")
    @DynamoDBTypeConverted(converter = DynamoDbLocalDateTimeConverter::class)
    var openingTime: LocalDateTime,
    var price: BigDecimal,
    @DynamoDBTypeConverted(converter = DynamoDbLocalDateTimeConverter::class)
    var createdAt: LocalDateTime
) {
    constructor(quoteDto: QuoteDto) : this(
        id = quoteDto.id,
        isin = quoteDto.isin,
        openingTime = quoteDto.openingTime,
        price = quoteDto.price,
        createdAt = quoteDto.createdAt
    )
}
