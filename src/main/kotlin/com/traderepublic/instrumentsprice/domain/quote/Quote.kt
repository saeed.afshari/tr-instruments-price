package com.traderepublic.instrumentsprice.domain.quote

import java.time.LocalDateTime

interface Quote {
    fun save(quoteDto: QuoteDto): QuoteDto
    fun deleteByISIN(isin: String)
    fun findAllByIsinAndOpeningTime(isin: String, openingTime: LocalDateTime): List<QuoteDto>
}
