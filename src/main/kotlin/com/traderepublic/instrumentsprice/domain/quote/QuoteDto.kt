package com.traderepublic.instrumentsprice.domain.quote

import java.math.BigDecimal
import java.time.LocalDateTime

data class QuoteDto(
    val id: String,
    val isin: String,
    val openingTime: LocalDateTime,
    val price: BigDecimal,
    val createdAt: LocalDateTime
) {
    constructor(quoteEntity: QuoteEntity) : this(
        id = quoteEntity.id,
        openingTime = quoteEntity.openingTime,
        isin = quoteEntity.isin,
        price = quoteEntity.price,
        createdAt = quoteEntity.createdAt
    )
}
