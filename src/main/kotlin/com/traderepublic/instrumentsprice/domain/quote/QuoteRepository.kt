package com.traderepublic.instrumentsprice.domain.quote

import org.socialsignin.spring.data.dynamodb.repository.DynamoDBCrudRepository
import java.time.LocalDateTime

interface QuoteRepository : DynamoDBCrudRepository<QuoteEntity, String> {
    fun deleteByIsin(isin: String)
    fun findByIsinAndOpeningTime(isin: String, openingTime: LocalDateTime): List<QuoteEntity>
}
