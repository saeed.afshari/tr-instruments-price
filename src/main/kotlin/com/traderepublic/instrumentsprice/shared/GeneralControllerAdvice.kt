package com.traderepublic.instrumentsprice.shared

import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.ControllerAdvice
import org.springframework.web.bind.annotation.ExceptionHandler
import org.springframework.web.context.request.WebRequest
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler


@ControllerAdvice
class GeneralControllerAdvice : ResponseEntityExceptionHandler() {

    @ExceptionHandler(value = [IllegalArgumentException::class, IllegalStateException::class])
    fun handle(exception: RuntimeException, request: WebRequest): ResponseEntity<Any> =
        ResponseEntity(exception.message, HttpStatus.NOT_FOUND)
}
