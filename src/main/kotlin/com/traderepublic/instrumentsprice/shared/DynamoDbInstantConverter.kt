package com.traderepublic.instrumentsprice.shared

import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBTypeConverter
import java.time.Instant

class DynamoDbInstantConverter : DynamoDBTypeConverter<String, Instant> {

    override fun convert(value: Instant?): String? = value?.toString()

    override fun unconvert(value: String?): Instant? = value?.let { Instant.parse(value) }
}
