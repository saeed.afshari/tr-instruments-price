package com.traderepublic.instrumentsprice.shared

@Target(AnnotationTarget.CLASS)
annotation class NoArgsConstructor
