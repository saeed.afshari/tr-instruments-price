package com.traderepublic.instrumentsprice.shared

import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBTypeConverter
import java.time.LocalDateTime

class DynamoDbLocalDateTimeConverter : DynamoDBTypeConverter<String, LocalDateTime> {

    override fun convert(value: LocalDateTime?): String? = value?.toString()

    override fun unconvert(value: String?): LocalDateTime? = value?.let { LocalDateTime.parse(value) }
}
