package com.traderepublic.instrumentsprice.usecase.candlesticks

import com.traderepublic.instrumentsprice.domain.instrument.Instrument
import com.traderepublic.instrumentsprice.domain.quote.Quote
import com.traderepublic.instrumentsprice.domain.quote.QuoteDto
import com.traderepublic.instrumentsprice.usecase.candlesticks.instrumentconsumer.InstrumentStreamService
import com.traderepublic.instrumentsprice.usecase.candlesticks.quoteconsumer.QuoteStreamService
import mu.KotlinLogging
import org.springframework.stereotype.Service
import java.time.LocalDateTime
import java.util.*
import javax.annotation.PostConstruct

@Service
class StreamHandlerService(
    private val instrumentStreamService: InstrumentStreamService,
    private val quoteStreamService: QuoteStreamService,
    private val instrumentService: Instrument,
    private val quoteService: Quote
) {
    private val logger = KotlinLogging.logger { }

    @PostConstruct
    fun start() {
        instrumentStreamService.connect {
            when (it.type) {
                "ADD" -> instrumentService.save(it.data)
                "DELETE" -> {
                    instrumentService.deleteByISIN(it.data.isin)
                    quoteService.deleteByISIN(it.data.isin)
                    logger.info { "Instrument deleted ${it.data.isin}" }
                }
                else -> logger.info { "No proper action found on the type=${it.type}" }
            }
        }

        quoteStreamService.connect {
            // As there is no timestamp in the quote event, the opening time is set to now - 30 minutes
            val openingTime = LocalDateTime.now().minusMinutes(30)
            quoteService.save(
                QuoteDto(
                    id = UUID.randomUUID().toString(),
                    isin = it.data.isin,
                    openingTime = openingTime,
                    price = it.data.price,
                    createdAt = openingTime
                )
            )
            logger.info { "Quote saved ${it.data.isin}->${it.data.price}" }
        }
    }
}
