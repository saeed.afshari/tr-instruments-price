package com.traderepublic.instrumentsprice.usecase.candlesticks

import java.math.BigDecimal
import java.time.LocalDateTime

data class CandlesticksDto(
    val openTimestamp: LocalDateTime,
    val openPrice: BigDecimal,
    val highPrice: BigDecimal,
    val lowPrice: BigDecimal,
    val closePrice: BigDecimal,
    val closeTimestamp: LocalDateTime
)
