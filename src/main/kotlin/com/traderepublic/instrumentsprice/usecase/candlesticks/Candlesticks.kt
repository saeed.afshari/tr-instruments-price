package com.traderepublic.instrumentsprice.usecase.candlesticks

interface Candlesticks {

    fun getCandlesticks(getCandlesticksCommandDto: GetCandlesticksCommandDto): List<CandlesticksDto>
}
