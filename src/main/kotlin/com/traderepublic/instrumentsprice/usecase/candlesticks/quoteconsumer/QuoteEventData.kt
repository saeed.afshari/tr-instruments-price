package com.traderepublic.instrumentsprice.usecase.candlesticks.quoteconsumer

import com.traderepublic.instrumentsprice.shared.NoArgsConstructor
import java.math.BigDecimal

@NoArgsConstructor
data class QuoteEventData(val isin: String, val price: BigDecimal)
