package com.traderepublic.instrumentsprice.usecase.candlesticks

import com.traderepublic.instrumentsprice.domain.quote.Quote
import com.traderepublic.instrumentsprice.domain.quote.QuoteDto
import org.springframework.stereotype.Service
import java.time.LocalDateTime

@Service
class CandlesticksService(val quoteService: Quote) : Candlesticks {

    override fun getCandlesticks(getCandlesticksCommandDto: GetCandlesticksCommandDto): List<CandlesticksDto> {
        val since = getCandlesticksCommandDto.since
        return when (getCandlesticksCommandDto.window) {
            CandlesticksWindow.THIRTY_MINUTES -> getThirtyMinutesWindowCandlesticks(
                since.toHourAndMinuteFormat(),
                getCandlesticksCommandDto.isin
            )
            else -> throw IllegalArgumentException("Candlesticks in the specified window timeframe is not implemented yet.")
        }
    }

    private fun getThirtyMinutesWindowCandlesticks(since: LocalDateTime, isin: String): List<CandlesticksDto> {
        val thirtyMinutesQuotesList = getLastThirtyMinutesQuotesList(since, isin)
        var lastMinuteQuoteDtoList: List<QuoteDto> = emptyList()
        return thirtyMinutesQuotesList.mapIndexedNotNull { index, quoteDtoList ->
            val result = if (quoteDtoList.isEmpty()) {
                if (lastMinuteQuoteDtoList.isEmpty() && index > 0) {
                    lastMinuteQuoteDtoList = thirtyMinutesQuotesList[index - 1]
                }
                lastMinuteQuoteDtoList.ifEmpty { null }
            } else {
                lastMinuteQuoteDtoList = quoteDtoList
                quoteDtoList
            }
            result?.toCandlesticks()
        }
    }

    private fun getLastThirtyMinutesQuotesList(
        since: LocalDateTime,
        isin: String
    ) = (0L..29).map { minute ->
        val openingTime = since.plusMinutes(minute)
        quoteService.findAllByIsinAndOpeningTime(isin, openingTime)
    }

    private fun List<QuoteDto>.toCandlesticks(): CandlesticksDto {
        val sortedQuoteList = this.sortedBy { it.createdAt }
        val firstQuote = sortedQuoteList.first()
        val openPrice = firstQuote.price
        val closePrice = sortedQuoteList.last().price
        val highPrice = sortedQuoteList.maxOf { it.price }
        val lowPrice = sortedQuoteList.minOf { it.price }
        val openTimestamp = firstQuote.createdAt.toHourAndMinuteFormat()
        return CandlesticksDto(
            openTimestamp = openTimestamp,
            openPrice = openPrice,
            highPrice = highPrice,
            lowPrice = lowPrice,
            closePrice = closePrice,
            closeTimestamp = openTimestamp.plusMinutes(1)
        )
    }

    private fun LocalDateTime.toHourAndMinuteFormat(): LocalDateTime = withSecond(0).withNano(0)
}
