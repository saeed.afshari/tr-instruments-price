package com.traderepublic.instrumentsprice.usecase.candlesticks

import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController
import java.time.LocalDateTime

@RestController
@RequestMapping("/api/v1/candlesticks")
class CandlesticksAPI(
    private val candlesticksService: Candlesticks
) {

    @GetMapping("{isin}")
    fun getLast30MinutesCandleSticks(@PathVariable isin: String): List<CandlesticksDto> {
        val candlesticksSince = LocalDateTime.now().minusMinutes(30)
        return candlesticksService.getCandlesticks(
            GetCandlesticksCommandDto(
                isin = isin,
                since = candlesticksSince,
                window = CandlesticksWindow.THIRTY_MINUTES
            )
        )
    }
}
