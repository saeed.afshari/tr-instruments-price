package com.traderepublic.instrumentsprice.usecase.candlesticks.instrumentconsumer

import com.traderepublic.instrumentsprice.domain.instrument.InstrumentDto
import com.traderepublic.instrumentsprice.shared.NoArgsConstructor

@NoArgsConstructor
data class InstrumentEvent(val type: String, val data: InstrumentDto)
