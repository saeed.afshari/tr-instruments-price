package com.traderepublic.instrumentsprice.usecase.candlesticks

import java.time.LocalDateTime

data class GetCandlesticksCommandDto(
    val isin: String,
    val since: LocalDateTime,
    val window: CandlesticksWindow
)
