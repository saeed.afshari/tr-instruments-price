package com.traderepublic.instrumentsprice.usecase.candlesticks.instrumentconsumer

import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.module.kotlin.readValue
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking
import mu.KotlinLogging
import org.http4k.client.WebsocketClient
import org.http4k.core.Uri
import org.http4k.websocket.Websocket
import org.springframework.beans.factory.annotation.Value
import org.springframework.stereotype.Service

@Service
class InstrumentStreamService(
    @Value("\${ws.instruments.uri}") private val uriString: String,
    private val mapper: ObjectMapper
) {

    private val logger = KotlinLogging.logger { }

    fun connect(onEvent: (InstrumentEvent) -> Unit) {
        val uri = Uri.of(uriString)
        val ws: Websocket = WebsocketClient.nonBlocking(uri) { logger.info { "Connected instruments stream" } }

        ws.onMessage {
            val event = mapper.readValue<InstrumentEvent>(it.body.stream)
            onEvent(event)
        }

        ws.onClose {
            logger.warn { "Disconnected instruments stream: ${it.code}; ${it.description}" }
            runBlocking {
                launch {
                    delay(5000L)
                    logger.info { "Attempting reconnect for instruments stream" }
                    connect(onEvent)
                }
            }
        }

        ws.onError {
            logger.error { "Exception in instruments stream: $it" }
        }
    }
}
