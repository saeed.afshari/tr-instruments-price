package com.traderepublic.instrumentsprice.usecase.candlesticks.quoteconsumer

import com.traderepublic.instrumentsprice.shared.NoArgsConstructor

@NoArgsConstructor
data class QuoteEvent(val data: QuoteEventData)
