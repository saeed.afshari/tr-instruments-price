package com.traderepublic.instrumentsprice.usecase.candlesticks

enum class CandlesticksWindow {
    ONE_MINUTE,
    TWO_MINUTES,
    FIVE_MINUTES,
    FIFTEEN_MINUTES,
    THIRTY_MINUTES
}
