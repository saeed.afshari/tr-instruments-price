import org.jetbrains.kotlin.gradle.tasks.KotlinCompile

plugins {
	id("org.springframework.boot") version "2.6.3"
	id("io.spring.dependency-management") version "1.0.11.RELEASE"
	kotlin("jvm") version "1.6.10"
	kotlin("plugin.spring") version "1.6.10"
	id("org.jetbrains.kotlin.plugin.noarg") version "1.6.10"
	id("org.jetbrains.kotlin.plugin.allopen")    version "1.6.10"
	id("com.github.johnrengelman.shadow") version "7.1.2"
}

group = "com.traderepublic"
version = "0.0.1-SNAPSHOT"
java.sourceCompatibility = JavaVersion.VERSION_11

repositories {
	mavenCentral()
}

dependencies {
	implementation("org.springframework.boot:spring-boot-starter-web")
	implementation("com.fasterxml.jackson.module:jackson-module-kotlin:2.13.1")
	implementation("com.fasterxml.jackson.datatype:jackson-datatype-jsr310:2.13.1")
	implementation("org.jetbrains.kotlin:kotlin-reflect")
	implementation("org.jetbrains.kotlin:kotlin-stdlib-jdk8")
	implementation("io.github.boostchicken:spring-data-dynamodb:5.2.5")
	implementation(platform("org.http4k:http4k-bom:4.19.3.0"))
	implementation("org.http4k:http4k-core")
	implementation("org.http4k:http4k-server-netty")
	implementation("org.http4k:http4k-client-websocket:4.19.3.0")
	implementation("org.http4k:http4k-format-jackson:4.19.3.0")
	implementation("io.github.microutils:kotlin-logging:2.1.21")
	implementation("org.jetbrains.kotlinx:kotlinx-coroutines-core:1.6.0")
	testImplementation("org.springframework.boot:spring-boot-starter-test")
	testImplementation("io.mockk:mockk:1.12.2")
	testImplementation("com.ninja-squad:springmockk:3.1.0")
}

tasks.withType<KotlinCompile> {
	kotlinOptions {
		freeCompilerArgs = listOf("-Xjsr305=strict")
		jvmTarget = "11"
	}
}

tasks.withType<Test> {
	useJUnitPlatform()
}

noArg {
	annotation("com.traderepublic.instrumentsprice.shared.NoArgsConstructor")
}

tasks.withType<com.github.jengelman.gradle.plugins.shadow.tasks.ShadowJar> {
	manifest {
		attributes["Main-Class"] = "InstrumentsPriceApplication"
	}
}
