# Getting Started

## Requirements

The following applications should be installed before launching the application

- Docker
- docker-compose
- Terraform
- jdk 11

## Launching the application

In order to launch the applications correctly the following commands should be executed in the root directory of the
project:

`
docker-compose -f infra/dynamodb/docker-compose.yml up -d
`

`
terraform -chdir=infra/dynamodb/tables init
`

`
terraform -chdir=infra/dynamodb/tables plan
`

`
terraform -chdir=infra/dynamodb/tables apply -auto-approve
`

`
docker-compose -f partner-service/docker-compose.yml up -d
`

`
./gradlew bootRun
`

### API
After successfully launching the application, the following endpoints will be active:
- localhost:9000/api/v1/candlesticks/{isin}
- localhost:8001 -> local dynamodb admin

## Database

As eventually the number of instruments can grow significantly, the selected database should be easily scaled. NoSQL
databases such as Amazon DynamoDB can scale better than any RDBMS. Therefore, for the challenge, AWS Dynamodb used.

### Database Tables

#### instruments

In order to keep the track of any instrument, a table called instruments will be created.

#### instruments indexes

- Hash key on isin

#### quotes

In order to group the consumed quotes in different timeframes, all the quotes would have been collected in a table,
called quotes.

#### quotes indexes

- Hash key on isin and sort key on openingTime

### Actions on the database tables

#### Inserting instruments

Per instrument ADD event, a new record instrument with the following attributes will be inserted into the instruments
table:
`isin | description | createdAt`

#### Deleting instruments

- Per instrument DELETE event, an instrument with the specified isin will be deleted from the instruments table.
- Per instrument DELETE event, all relevant quotes to the specified isin will be deleted from the quotes table.

#### Inserting quotes

Per quote event, a new record with the following attributes will be inserted into the quotes table:
`isin | openingTime | createdAt | price`

#### Querying quotes in 30 minutes window timeframe

Per minute in a 30 minutes window timeframe, the data from the quotes table with the following list of ids will be
queried:

`[(isin, minute1 timestamp) ... (isin, minute30 timestamp)]`

### Core Business Logic

The output (and the main task of this challenge) is the aggregated price history endpoint. It should provide a 30
minutes quotes history in the form of minute candlesticks (check information below) for any requested instrument.

#### Implementation

- In a 30-minutes window, fetch all the quotes from the quotes table
- Group results by openingTime
- Map each grouping result to a CandlestickDto

Assumptions:
- As there is no timestamp in quote streams, LocalDateTime.now.minusMinutes(30) will be taking into account as the timestamp for the quote event.
